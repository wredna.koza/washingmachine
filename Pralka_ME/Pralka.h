#pragma once

#include <thread>
#include <vector>
#include <fstream>
#include <sstream>

#include "Program.h"

class Pralka {
public:
    std::vector<Program> PROGRAM_LIST;
    Program PROGRAM_RESUMED;
    void powerOn();

private:
    enum State {
        STATE_RESUME,
        STATE_IDLE,
        STATE_IDLE_CHOOSE,
        STATE_RUNNING,
        STATE_PAUSED
    };

    std::string line;
    std::fstream file;

    bool power{false};
    std::thread menuThread;

    State state{STATE_IDLE}; 
    Program defaultProgram;
    Program activeProgram;
    void runProgram(int prog);

    void powerOff();
    void printMenu();
    void printStatus();
    void handleInput();
    void doWork();
    void readMamory();  
    void saveStatus();
    void readSaved();
};
