#include <iostream>
#include <thread>

#include "Pralka.h"

using namespace std::chrono_literals;

void Pralka::doWork() {
    while (power) {
        if (state == STATE_RUNNING) {
            activeProgram.step();
            if (activeProgram.getRemainingTime() <= 0) {
                std::cout << "\nProgram finished: " << activeProgram.print() << "\n" << std::flush;
                activeProgram = defaultProgram;
                saveStatus();
                state = STATE_IDLE;
            }
        }
        std::this_thread::sleep_for(1s);
        printStatus();
    }
}

void Pralka::powerOn() {
    readSaved();
    readMamory();
    
    power = true;
    menuThread = std::thread( [this] { handleInput(); });

    doWork();

    menuThread.join();
}

void Pralka::powerOff() {
    power = false;
}

void Pralka::runProgram(int prog) {
    activeProgram = Pralka::PROGRAM_LIST.at(prog);
    std::cout << "Running program: " << activeProgram.print() << std::flush;
}

void Pralka::printMenu() {
    switch (state) {
    case STATE_IDLE: {
        std::cout << "Select option: h - choose program, q - power off, x - resume from memory\n";
        break;
    }
    case STATE_IDLE_CHOOSE: {
        std::cout << "\nSelect program:";
        char menuOpt = 'a';
        for (auto prog : Pralka::PROGRAM_LIST) {
            std::cout << "\n" << menuOpt << ") " << prog.print();
            menuOpt++;
        }
        std::cout << "\n";
        break;
    }
    case STATE_RUNNING: {
        std::cout << "\nSelect option: p - pause program, s - stop program, q - power off\n";
        break;
    }
    case STATE_PAUSED: {
        std::cout << "\nSelect option: r - resume program, s - stop program, q - power off\n";
        break;
    }
    default:
        std::cout << "\nWrong menu state";
    }
}

void Pralka::printStatus() {
    std::ostringstream oss;
    oss << "status: ";
    switch (state) {
        case STATE_IDLE: {
            oss << "IDLE";
            break;
        }
        case STATE_IDLE_CHOOSE: {
            oss << "IDLE - CHOOSING PROGRAM";
            break;
        }
        case STATE_RUNNING: {
            oss << "RUNNING PROGRAM: " << activeProgram.print();
            break;
        }
        case STATE_PAUSED: {
            oss << "PROGRAM PAUSED: " << activeProgram.print();
            break;
        }
        case STATE_RESUME: {
            oss << "RESUMING FROM MEMORY... " << activeProgram.print();
            break;
        }
        default: {
            std::cout << "Invalid state, aborting" << std::flush;
            powerOff();
        }
    }

    std::cout << "\r";
    std::cout << oss.str() << " " << std::flush;
}

void Pralka::handleInput() {
    do {
        printMenu();
        char input = 'b';
        std::cin >> input;
        switch (input) {
            case 'a': {
                runProgram(0);
                state = STATE_RUNNING;
                break;
            }
            case 'b': {
                runProgram(1);
                state = STATE_RUNNING;
                break;
            }
            case 'c': {
                runProgram(2);
                state = STATE_RUNNING;
                break;
            }
            case 'd': {
                runProgram(3);
                state = STATE_RUNNING;
                break;
            }
            case 'e': {
                runProgram(4);
                state = STATE_RUNNING;
                break;
            }
            case 'f': {
                runProgram(5);
                state = STATE_RUNNING;
                break;
            }
            case 'h': {
                state = STATE_IDLE_CHOOSE;
                break;
            }
            case 's': {
                std::cout << "Stopping program ...\n";
                activeProgram = Pralka::defaultProgram;
                saveStatus();
                state = STATE_IDLE;
                break;
            }
            case 'p': {
                std::cout << "Pausing ...\n";
                state = STATE_PAUSED;
                saveStatus();
                break;
            }
            case 'r': {
                std::cout << "Resuming operation ...\n";
                state = STATE_RUNNING;
                break;
            }
            case 'q': {
                std::cout << "Powering off ...\n";
                saveStatus();
                powerOff();
                break;
            }
            case 'x': {
                activeProgram = PROGRAM_RESUMED;
                state = STATE_PAUSED;
                break;
            }
            default:
                std::cout << "Wrong option selected\n";
        }
    } while(power);
}
void Pralka::readSaved() {
    file.open("saved.txt", std::ios::in);
    if(file.good() == true){
            std::string tit; int temp; int dur; float water;
            file >> tit; 
            file >> temp;
            file >> dur;
            file >> water;
            PROGRAM_RESUMED = Program({tit, temp, dur, water});
             }
    file.close();
}
void Pralka::readMamory() {
    file.open("programs.txt", std::ios::in);
    if(file.good() == true){
            for (int j=0; j < 5; ++j) {
                std::string tit; int temp; int dur; float water;
                file >> tit; 
                file >> temp;
                file >> dur;
                file >> water;
                PROGRAM_LIST.push_back({tit, temp, dur, water});
        }
    file.close();
    }
}
void Pralka::saveStatus() {
    file.open( "saved.txt", std::ios::in | std::ios::out | std::fstream::trunc);
    if( file.good() == true ) {
        file << activeProgram.printToFirst();
        file.close();
    }
    else std::cout << "File access forbidden \n";
}
