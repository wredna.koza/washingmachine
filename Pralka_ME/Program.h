#pragma once

#include <sstream>

class Program {
public:
    Program(){};
    Program(std::string tit, int temp, int dur, float water) : 
        title(tit),temperature(temp), duration(dur), remainingTime(dur), waterConsumption(water){};

    std::string print() {
        std::ostringstream oss;
        oss << title << ", Temperture: " << temperature << "*C " << ", Duration: " << duration << ", Water: " << waterConsumption <<
            " Remaining: " << remainingTime;
        return oss.str();
    };
    std::string printToFirst() {
        std::ostringstream first;
        first << title << " " << temperature << " " << remainingTime << " " << waterConsumption << "\n";
        return first.str();
    }
    void step() { if (remainingTime > 0) remainingTime--; };
    int getRemainingTime() { return remainingTime; };

private:
    std::string title{"NONE"};
    int temperature{0};
    int duration{0};
    int remainingTime{0};
    float waterConsumption{0};
    float powerConsumption{0};
};
